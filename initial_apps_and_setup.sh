#!/bin/sh

echo download openjdk

sudo apt-get install openjdk-11-jdk

echo openjdk is downloaded

echo _________________________________

echo copy renew_all.sh to /usr/local/bin
sudo cp renew_all.sh /usr/local/bin

echo _________________________________

echo set executable renew_all.sh
sudo chmod 777 /usr/local/bin/renew_all.sh

echo _________________________________

echo copy renew_jetbrains.service to /etc/systemd/system
sudo cp renew_jetbrains.service /etc/systemd/system

echo _________________________________

echo set executable renew_jetbrains.service
sudo chmod 777 /etc/systemd/system/renew_jetbrains.service

echo ________________________________
echo daemon-reload and enable service 
sudo systemctl daemon-reload
sudo systemctl enable renew_jetbrains.service

echo _________________________________

echo git configs
git config --global alias.lga "log --graph --oneline --all --decorate"
git config --global core.editor "vim"
git config --global user.name "Eduard Matveev"
git config --global user.email "dev.eduardmatveev@gmail.com"

echo donwload and install pycharm from snap
sudo snap install pycharm-professional --channel=2020.3/stable --classic

echo donwload and install intellij from snap
sudo snap install intellij-idea-ultimate --channel=2020.3/stable --classic

echo donwload and install goland from snap
sudo snap install goland --channel=2020.3/stable --classic

echo download and install telegram desktop
sudo snap install telegram-desktop

echo download and install brave
sudo snap install brave

echo DISABLE AUTOUPDATE
echo DISABLE AUTOUPDATE
echo DISABLE AUTOUPDATE
echo DISBALE AUTOUPDATE
echo DISABLE AUTOUPDATE
echo DISABLE AUTOUPDATE
echo DISABLE AUTOUPDATE
echo DISABLE AUTOUPDATE
echo DISABLE AUTOUPDATE
echo DISABLE AUTOUPDATE
echo DISABLE AUTOUPDATE
echo DISABLE AUTOUPDATE
