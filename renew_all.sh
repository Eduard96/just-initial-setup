#!/bin/sh

echo "you are getting evaluation:)"
echo "Preparing evalutation file"

rm -r ~/.config/JetBrains/IntelliJIdea*/eval
rm -r ~/.config/JetBrains/PyCharm*/eval
rm -r ~/.config/JetBrains/GoLand*/eval
echo "25% is Done"
rm -r ~/.config/JetBrains/IntelliJIdea*/options/other.xml
rm -r ~/.config/JetBrains/PyCharm*/options/other.xml
rm -r ~/.config/JetBrains/GoLand*/options/other.xml
echo "50% is Done"
sudo sed  -i -E 's/<property name=\"evl.*\".*\/>//' ~/.config/JetBrains/Web*/options/other.xml
sudo sed  -i -E 's/<property name=\"evl.*\".*\/>//' ~/.config/JetBrains/PyCharm*/options/other.xml
sudo sed  -i -E 's/<property name=\"evl.*\".*\/>//' ~/.config/JetBrains/GoLand*/options/other.xml
echo "75% is Done"
rm -r ~/.java/.userPrefs/jetbrains/idea
rm -r ~/.java/.userPrefs/jetbrains/pycharm
rm -r ~/.java/.userPrefs/jetbrains/goland
echo "100% is Done"
echo "EVALUATION HAS COMPLETED SUCCESSFULY"
